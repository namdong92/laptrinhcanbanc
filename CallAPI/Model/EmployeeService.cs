﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CallAPI 
{
    class EmployeeService
    {

        public async Task<List<Employee>> GetAll()
        {
            try
            {
                using (var client = new HttpClient())
                {

                    HttpResponseMessage response = await client.GetAsync("https://nguyenvando.net/api/getLocation/");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    List<Employee> articles = JsonConvert.DeserializeObject<List<Employee>>(responseBody);

                    return articles;
                }
            }
            catch (HttpRequestException e)
            {
                throw new Exception(e.Message);
            }

        }
    }
}
