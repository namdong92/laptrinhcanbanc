﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CallAPI
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Parent { get; set; }
        public int isShow { get; set; }
        public string Note { get; set; }
        public string Alias { get; set; }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4}", Id, Name, Parent, isShow, Note, Alias);
        }


        public string CustomerById(string id) {
            return Get("https://api.vkhealth.vn/api/BkCustomer/GetById/" + id);
        }

        public string Get(string stringPath)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    string model = null;
                    var task = client.GetAsync(stringPath)
                      .ContinueWith((taskwithresponse) =>
                      {
                          var response = taskwithresponse.Result;
                          var jsonString = response.Content.ReadAsStringAsync();
                          jsonString.Wait();
                          model = jsonString.Result;
                      });
                    task.Wait();
                    return model;

                }
            }
            catch (HttpRequestException e)
            {
                throw new HttpRequestException(e.Message);
            }
 
        }

        public string testPost()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Tokencuaban");
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    string model = null;
                    var content = new FormUrlEncodedContent(new[]
                       {
                            new KeyValuePair<string, string>("user", "namdong"),
                            new KeyValuePair<string, string>("Password", "123456")

                        });
                    var task = client.PostAsync("https://nguyenvando.net/api/testPost", content)
                        .ContinueWith((taskwithresponse) =>
                      {
                          var response = taskwithresponse.Result;
                          var jsonString = response.Content.ReadAsStringAsync();
                          jsonString.Wait();
                          model = jsonString.Result;
                      });
                    task.Wait();
                    return model;

                }
            }
            catch (HttpRequestException e)
            {
                throw new HttpRequestException(e.Message);
            }

        }


        public List<Employee> ApiCall()
        {
            try
            {
                using (var client = new HttpClient())
                {

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    List<Employee> model = null;
                    var task =  client.GetAsync("https://nguyenvando.net/api/getLocation/")
                      .ContinueWith((taskwithresponse) =>
                      {
                          var response = taskwithresponse.Result;
                          var jsonString = response.Content.ReadAsStringAsync();
                          jsonString.Wait();
                          model = JsonConvert.DeserializeObject<List<Employee>>(jsonString.Result);

                      });
                    task.Wait();
                    return model;

                }
            }
            catch (HttpRequestException e)
            {
                throw new HttpRequestException(e.Message);
            }

        }

    }
}
