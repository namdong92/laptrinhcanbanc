using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LapTrinhCanBanTT
{
    struct SinhVien {

        public static  List<SinhVien> DSSinhVien { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime NgaySinh { get; set; }
        public string  SDT { get; set; }

        public string Email { get; set; }



        public SinhVien(int id, 
            string name, 
            DateTime ngaySinh, 
            string sDT, 
            string email) : this()
        {
            if (DSSinhVien == null) {
                DSSinhVien = new List<SinhVien>();
            }
            Id = id;
            Name = name;
            NgaySinh = ngaySinh;
            SDT = sDT;
            Email = email;
        }

        public string ThongTinSinhVien()
        {
            return String.Format("{0}|{1}|{2}|{3}|{4}",
                this.Id,this.Name,
                this.NgaySinh,this.SDT,this.Email
                );
        }
        /// <summary>
        /// them sinh vien vao danh sach
        /// </summary>
        /// <param name="sv1"></param>
        public void ThemSinhVien(SinhVien sv1 = new SinhVien())
        {
            SinhVien timSv;
            if (sv1.Id == 0)
                timSv = this;
            else
                timSv = sv1;

            bool timKiemsv = CheckSinhVienById(timSv.Id);
            if (timKiemsv == false)
                DSSinhVien.Add(timSv);
        }

        private bool CheckSinhVienById(int id)
        {
            foreach (SinhVien item in DSSinhVien)
            {
                if (item.Id == id)
                {
                    return true;
                }
            }
            return false;
        }
    }

    class Program
    {



        static void Main(string[] args)
        {
            #region list 
            //List<int> li = new List<int>();

            //li.Add(3);
            //li.Add(4);
            //li.Add(6);
            //li.Add(7);
            //li.Add(7);
            //li.Add(7);
            //li.Add(7);
            //li.Add(7);
            //li.Add(7);
            //li.Add(7);

            //for (int i = 0; i < li.Count; i++)
            //{
            //    Console.WriteLine("Pt {0}", li[i]);
            //}

            //foreach (var item in li)
            //{
            //    Console.WriteLine("Pt {0}", item);
            //}


            //Console.ReadKey();
            #endregion

            SinhVien sv = new SinhVien(1, "teo nguyen"
                , new DateTime(2000, 1, 1)
                , "0123456789"
                , "teonguyen@gmail.com");
            SinhVien sv1 = new SinhVien(2, "ti nguyen"
                , new DateTime(2000, 1, 1)
                , "0123456789"
                , "teonguyen@gmail.com");

            sv.ThemSinhVien();
            sv.ThemSinhVien();

            sv.ThemSinhVien(sv1);

            foreach (SinhVien item in SinhVien.DSSinhVien)
            {
                Console.WriteLine(item.ThongTinSinhVien());
            }
 

            //Console.WriteLine(sv.Name);
            //string thongTinSinhVien = sv.ThongTinSinhVien();
            //Console.WriteLine(thongTinSinhVien);


            // XinChao();
            //XinChao2();
            //XinChao2("abc");
            //XinChao();
            // XacDinhSoChanNhapTuBanPhim();
            // int gt = TinhGiaiThua(3);
            // Menu();

        }

        private static int TinhGiaiThua(int n)
        {
            if (n == 1)
                return 1;
            return n * TinhGiaiThua(n - 1);
        }

        private static void Menu()
        {

            Console.WriteLine("1 : Xac dinh so chan");
            Console.WriteLine("2 : Giai Phuong Trinh Bac 1");
            Console.WriteLine("3 : Giai Phuong Trinh Bac 2");
            Console.WriteLine("4 : Hinh Vuong");
            Console.WriteLine("Chon:");
            string chon = Console.ReadLine();
            if (chon == "exit")
            {
                return;
            }
            switch (chon)
            {
                case "1":
                    XacDinhSoChanNhapTuBanPhim();
                    break;
                case "2":
                    GiaiPhuongTrinhBac1();
                    break;
                case "3":
                    GiaiPhuongTrinhBac2();
                    break;
                case "4":
                    HinhVuong();
                    break;

            }
            Menu();
        }




        private static void HinhVuong()
        {
            double a = NhapSoTuBanPhim("Nhap canh hinh vuong");
            double dienTich= 0,chuVi =0;
            TinhChatHinhVuong(a ,ref dienTich, ref chuVi);
            TinhChatHinhVuongOut(a, out dienTich, out chuVi);
            Console.WriteLine(@"Chu vi: 
{0},Dien Tich: {1}", chuVi, dienTich);
        }

        private static void TinhChatHinhVuongOut(double a, out double dienTich, out double chuVi)
        {
            dienTich = 0;
            chuVi = 0;
        }

        private static void TinhChatHinhVuong(double a, ref double dienTich, ref double chuVi)
        {
            dienTich = a * a;
            chuVi = a * 4;
        }

        private static void GiaiPhuongTrinhBac2()
        {
            double a = NhapSoTuBanPhim("Nhap so a:");
            double b = NhapSoTuBanPhim("Nhap so b:");
            double c = NhapSoTuBanPhim("Nhap so c:");
            try
            {
                double[] nghiem = GiaiPTB2(a, b, c);
          Console.WriteLine(@"Nghiem cua pt 
: x1={0},x2 ={1} " ,nghiem[0],nghiem[1]);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            



        }

        private static void GiaiPhuongTrinhBac1()
        {
            // 2x + 3 = 0 ;
            double a = NhapSoTuBanPhim("Nhap so a");
            double b = NhapSoTuBanPhim("Nhap so b");
            try
            {
                double x = GiaiPTB1(a, b);
                Console.WriteLine("nghiem cua pt la: {0}", x);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }
        /// <summary>
        /// nhap so tu ban phim 
        /// </summary>
        /// <param name="v">Messes</param>
        /// <returns></returns>
        private static double NhapSoTuBanPhim(string v)
        {
            Console.WriteLine(v);
            bool kt = false;
            double a;
            do
            {
                kt = double.TryParse(Console.ReadLine(), out a);
                if (kt == true)
                    break;
                Console.WriteLine(v);
            } while (true);
            return a;
        }
        private static double[] GiaiPTB2(double a, double b ,double c)
        {
            double[] nghiem = { 0, 0 };
            // aX^2 + bx + c = 0;
            if (a == 0) {
                nghiem[0] = nghiem[1] = GiaiPTB1(b,c);
            }
            double deta = Math.Pow(b, 2) - 4 * a * c;
            if(deta < 0)
            {
                throw new Exception("Pt Vo nghiem");
            }
            nghiem[0] = (-b + Math.Sqrt(deta)) / (2 * a);
            nghiem[1] = (-b - Math.Sqrt(deta)) / (2 * a);

            return nghiem;
        }


        private static double GiaiPTB1(double a, double b)
        {
            if (a == 0)
            {
                if (b == 0)
                {
                    throw new Exception("Phuong Trinh Vo So Nghiem");
                }
                else
                {
                    throw new Exception("Phuong Trinh Vo Nghiem");
                }

            }
            return (-b / a);
        }

        private static void XacDinhSoChanNhapTuBanPhim()
        {
            int soNguyen = NhapSoNguyenDuong();
            bool kt = XacDinhSoChan(soNguyen);
            if (kt == true)
                Console.WriteLine("{0} la so chan", soNguyen);
            else
                Console.WriteLine("{0} la so le", soNguyen);
        }


        /// <summary>
        /// nhap so nguyen duong tu ban phim
        /// </summary>
        /// <returns></returns>
        private static int NhapSoNguyenDuong()
        {
            int a;
            bool kt = false;
            Console.WriteLine("Nhap so nguyen duong");

            do
            {
                kt = int.TryParse(Console.ReadLine(), out a);
                if (kt == true)
                {
                    if (a <= 0)
                    {
                        kt = false;
                    }
                }
                if (kt == false)
                {
                    Console.WriteLine("Nhap lai so nguyen duong");
                }
            } while (kt == false);
            return a;
        }

        /// <summary>
        /// xac dinh tinh chan le cua 1 so nguyen duong
        /// </summary>
        /// <param name="v">so nguyen duong</param>
        /// <returns></returns>
        private static bool XacDinhSoChan(int v)
        {
            return (v % 2) == 0 ? true : false;

        }

        /// <summary>
        /// ham nay xuat cau lenh xin chao
        /// </summary>
        /// <param name="v">ho ten nguoi</param>
        private static void XinChao2(string v)
        {
            Console.WriteLine("Xin Chao: {0}", v);
        }

        private static void XinChao2()
        {
            Console.WriteLine("Xin Chao");
        }

        //private static void XinChao(string v)
        //{
        //    Console.WriteLine("Xin Chao: {0}",v);
        //}

        static void XinChao(string v = "")
        {
            // code
            if (v == "")
                Console.WriteLine("Xin Chao");
            else
                Console.WriteLine("Xin Chao: {0}", v);
        }

    }
}
