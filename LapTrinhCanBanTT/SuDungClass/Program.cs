using SuDungClass.ApiLocation;
using SuDungClass.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class Program
    {
        static void Main(string[] args)
        {

            //           QuanLyNhanVien();
            try
            {
                SendMail m = new SendMail();
                m.Send();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            


            //LocationService Ls = new LocationService();

            //string s =  Ls.TestPost();

            //Console.WriteLine(s);
            //List<Location> lls = Ls.GetAll();

            //foreach (var item in lls)
            //{
            //    Console.WriteLine(item.ToString());
            //}
                

            Console.ReadKey();







        }

        private static void QuanLyNhanVien()
        {
            NhanVien nv = new NhanVien();
            nv.ID = "nv001";
            nv.Name = "nv001";
            nv.Phone = "nv001";
            nv.Brithdate = new DateTime(2000, 2, 28);

            NhanVien Nv2 = new NhanVien(
                "Nv002", "teo", "01213457",
                "hcm", new DateTime(2000, 2, 28)
                );
            NhanVien Nv3 = new NhanVien
            {
                ID = "Mv003",
                Name = "Mv003",
                Phone = "Mv003",
                Address = "HaNoi",
                Brithdate = new DateTime(2000, 2, 28)
            };

            NhanVienService nvs = new NhanVienService();
            nvs.ThemNhanVien(nv);
            nvs.ThemNhanVien(Nv2);
            nvs.ThemNhanVien(Nv3);

            bool kt = nvs.XoaNhanVien("Mv003");

            Nv2.Name = "ti";
            kt = nvs.SuaNhanVien(Nv2);


            NhanVien timNv =
                nvs.TimNhanVienTheoMa("Mv003");

            Console.WriteLine(timNv.ToString());

            foreach (NhanVien item in nvs.DanhSachNhanVien())
            {
                Console.WriteLine(item.ToString());
            }


        }
    }
}
