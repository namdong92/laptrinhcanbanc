﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    interface iNhanVienData
    {
        List<NhanVien> GetAll();

        NhanVien GetById(string Id);
        bool Create(NhanVien ModelNV);
        bool Update(NhanVien ModelNV);
        bool Delete(string Id);
    }
}
