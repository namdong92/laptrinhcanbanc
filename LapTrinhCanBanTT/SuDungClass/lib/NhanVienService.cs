﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class NhanVienService : NhanVienDataCode
    {
        public NhanVienService()
        {
        }

        public bool ThemNhanVien(NhanVien nv) {
            return this.Create(nv);
        }
        public List<NhanVien> DanhSachNhanVien()
        {
            return this.GetAll();
        }

        public bool XoaNhanVien(string v)
        {
            return Delete(v);
        }

        public bool SuaNhanVien(NhanVien nv2)
        {
            return Update(nv2);
        }

        public NhanVien TimNhanVienTheoMa(string v)
        {
            return GetById(v);
        }
    }
}
