﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class NhanVien
    {
        public string ID { get; set; } //vp001
        public string Name { get; set; } //vp001
        public string Phone { get; set; } //vp001
        public string Address { get; set; } //vp001
        public DateTime Brithdate { get; set; } //vp001

        public NhanVien(string iD,
            string name, 
            string phone, 
            string address, 
            DateTime brithdate)
        {
            ID = iD;
            Name = name;
            Phone = phone;
            Address = address;
            Brithdate = brithdate;
        }

        public override string ToString()
        {
            return String.Format("{0},{1},{2},{3},{4}",
                ID, Name, Phone, Address, Brithdate); ;
        }

        public NhanVien()
        {
        }
    }
}
