﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class NhanVienDataCode : iNhanVienData
    {
        public static List<NhanVien> DSNhanVien;



        public bool Create(NhanVien ModelNV)
        {
            if (DSNhanVien == null)
                DSNhanVien = new List<NhanVien>();

            DSNhanVien.Add(ModelNV);
            return true;
        }

        public bool Delete(string Id)
        {
            DSNhanVien.RemoveAll(item => item.ID == Id);
            return true;
        }

        public List<NhanVien> GetAll()
        {
            if (DSNhanVien == null)
                DSNhanVien = new List<NhanVien>();
            return DSNhanVien;
        }

        public NhanVien GetById(string Id)
        {
            foreach (var item in DSNhanVien)
                if (item.ID == Id)
                    return item;
            return new NhanVien();

        }

        public bool Update(NhanVien ModelNV)
        {
            this.Delete(ModelNV.ID);
            this.Create(ModelNV);
            return true;
        }
    }
}
