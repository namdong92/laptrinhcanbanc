﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace SuDungClass.ApiLocation
{
    class LocationService
    {
        public List<Location> GetAll() {

            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    List<Location> model = null;
                    var task = client.GetAsync("https://nguyenvando.net/api/getLocation/")
                        .ContinueWith(
                        (twepon) =>
                        {
                            var response = twepon.Result;
                            var jsonString = response.Content.ReadAsStringAsync();
                            jsonString.Wait();
                            model = JsonConvert.DeserializeObject<List<Location>>(jsonString.Result);
                        });
                    task.Wait();
                    return model;
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message) ;
            }
        
        }

        public string TestPost()
        {

            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Tokencuaban");
                    string model = null;

                    var content = new FormUrlEncodedContent(new[]
                       {
                            new KeyValuePair<string, string>("user", "namdong"),
                            new KeyValuePair<string, string>("Password", "123456")

                        });

                    var task = client.PostAsync("https://nguyenvando.net/api/testPost/",content)
                        .ContinueWith(
                        (twepon) =>
                        {
                            var response = twepon.Result;
                            var jsonString = response.Content.ReadAsStringAsync();
                            jsonString.Wait();
                            model = jsonString.Result;
                        });
                    task.Wait();
                    return model;
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

    }
}
