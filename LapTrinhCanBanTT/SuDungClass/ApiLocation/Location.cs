﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass.ApiLocation
{
    partial class Location : abCanh
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public string Parent { set; get; }
        public string isShow { set; get; }
        public string Alias { set; get; }

        public override string LoaiCanh()
        {
            return "canh long vu";
        }

        public override string MauCanh()
        {
            return "Mau Vang";
        }

        public override string ToString()
        {
            return string.Format("{0},{1}",Id,Name);
        }
    }
}
