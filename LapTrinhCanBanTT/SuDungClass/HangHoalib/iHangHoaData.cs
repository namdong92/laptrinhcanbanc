﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    interface iHangHoaData
    {
        List<HangHoa> GetAll();
        HangHoa GetById(string Id);
        bool Create(HangHoa ModelNV);
        bool Update(HangHoa ModelNV);
        bool Delete(string Id);

    }
}
