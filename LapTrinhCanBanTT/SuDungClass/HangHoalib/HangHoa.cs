﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class HangHoa
    {
        public string Ma { get; set; }
        public string Ten6 { get; set; }
        public double Gia { get; set; }
        public int SoLuong { get; set; }
    }
}
