﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class HinhTron : IHInhHoc
    {
        public double BanKinh { get; set; }

        public double ChuVi()
        {
            return (BanKinh * 2) * Math.PI;
        }

        public double DienTich()
        {
            return Math.Pow(BanKinh, 2) * Math.PI;
        }
    }
}
