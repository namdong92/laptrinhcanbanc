﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass
{
    class HinhChuNhat : IHInhHoc
    {
        public double Canha { get; set; }
        public double Canhb { get; set; }

        public double ChuVi()
        {
            return (Canha + Canhb) * 2;
        }

        public double DienTich()
        {
            return Canhb * Canha;
        }
    }
}
