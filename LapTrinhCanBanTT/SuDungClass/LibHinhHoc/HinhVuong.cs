﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass.HinhHoc
{
     class HinhVuong : IHInhHoc
    {

        public double Canh { get; set; }

        public double DienTich() {
            return Canh * Canh;
        }
        public double ChuVi()
        {
            return Canh * 4;
        }
    }
}
