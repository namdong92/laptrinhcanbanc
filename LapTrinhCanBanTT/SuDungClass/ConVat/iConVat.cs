﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuDungClass.ConVat
{
    interface iConVat
    {
        int SoChan();
        string MauLong();

        double ChieuCao();

    }
}
