﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WFDataBase
{
    public partial class Form1 : Form
    {
        static Form form1;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'myDataDataSet.NhanVien' table. You can move, or remove it, as needed.
            this.nhanVienTableAdapter.Fill(this.myDataDataSet.NhanVien);

        }

        private void thêmNhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            form1 = this;
            
            Form f1 = new formNhanVien(form1);
            f1.Show();
            
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            Form1_Load(sender, e);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {

            try
            {
                ModelCreateNhanVien nvMoi = LayThongTinNhanVienTrenForm();
                NhanVienService nv = new NhanVienService();
                nv.ThemNhanVien(nvMoi);
                MessageBox.Show("Đã Thêm Nhân Viên", "Thông Báo");
                Form1_Load(sender,e);
                SetInputForm(new NhanVien());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }

        }
        private ModelCreateNhanVien LayThongTinNhanVienTrenForm()
        {
            ModelCreateNhanVien nvA = new ModelCreateNhanVien();
            nvA.Name = txtName.Text.Trim();
            if (nvA.Name == "")
            {
                txtName.Focus();
                throw new Exception("Tên Không Để Trống");
            }
            if (nvA.Name.Length > 50)
            {
                txtName.Focus();
                txtName.SelectAll();
                throw new Exception("Tên Quá Dài");
            }
            nvA.Email = txtEmail.Text.Trim();
            nvA.Phone = txtPhone.Text.Trim();
            nvA.Username = txtUsername.Text.Trim();
            nvA.Password = txtPassword.Text.Trim();
            return nvA;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NhanVienService nv = new NhanVienService();
            List<NhanVien> DSNhanVien =  nv.TimNhanVienTheoTen(txtTim.Text);
            
            dataGridView1.DataSource = DSNhanVien;
        }

        private void txtTim_TextChanged(object sender, EventArgs e)
        {
            if (txtTim.Text.Length > 2) {
                button1_Click(sender, e);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString().Trim();
            var name = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString().Trim();
            var phone = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString().Trim();
            var email = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString().Trim();
            var username = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString().Trim();
            var password = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString().Trim();
            NhanVien nv = new NhanVien(int.Parse(id), 
                name, phone, email, password, username);
            SetInputForm(nv);
        }

        private void SetInputForm(NhanVien nv)
        {
            txtId.Text = nv.Id.ToString();
            txtName.Text = nv.Name;
            txtPhone.Text = nv.Phone;
            txtEmail.Text = nv.Email;
            txtUsername.Text = nv.Username;
            txtPassword.Text = nv.Password;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            NhanVien nvCapNhat = GetInputFormNhanVien();
            NhanVienService nvs = new NhanVienService();
            nvs.SuaNhanVien(nvCapNhat);
            Form1_Load(sender, e);
            SetInputForm(new NhanVien());
            try
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private NhanVien GetInputFormNhanVien()
        {
            NhanVien nvA = new NhanVien();
            if (txtId.Text == "") {
                throw new Exception("Bạn chưa chọn nhân viên");
            }
            nvA.Id = int.Parse(txtId.Text);


            nvA.Name = txtName.Text.Trim();
            if (nvA.Name == "")
            {
                txtName.Focus();
                throw new Exception("Tên Không Để Trống");
            }
            if (nvA.Name.Length > 50)
            {
                txtName.Focus();
                txtName.SelectAll();
                throw new Exception("Tên Quá Dài");
            }
            nvA.Email = txtEmail.Text.Trim();
            nvA.Phone = txtPhone.Text.Trim();
            nvA.Username = txtUsername.Text.Trim();
            nvA.Password = txtPassword.Text.Trim();
            return nvA;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var id = txtId.Text;
            var isOk = MessageBox.Show("Bạn có muốn xóa nhân viên này không?", "Thông Báo",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (isOk == DialogResult.OK) {
                try
                {

                    NhanVienService nvs = new NhanVienService();
                    nvs.XoaNhanVien(id);
                    SetInputForm(new NhanVien());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            Form1_Load(sender, e);
        }

        private void bảngHàngHóaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fHangHoa = new formHangHoa();
            fHangHoa.Show();
        }
    }
}
