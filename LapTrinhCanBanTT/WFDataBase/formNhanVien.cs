﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WFDataBase
{
    public partial class formNhanVien : Form
    {
        private static Form Form1;

        public formNhanVien()
        {
            InitializeComponent();
        }

        public formNhanVien(Form form1)
        {
            Form1 = form1;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formNhanVien_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult kt = MessageBox.Show("Bạn đang làm việc với form nhân viên. bạn có muốn thoát không?"
                , "Thông Báo", MessageBoxButtons.OKCancel
                , MessageBoxIcon.Warning);

            if (kt != DialogResult.OK)
            {
                e.Cancel = true;
            }


        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                ModelCreateNhanVien nvMoi = LayThongTinNhanVienTrenForm();
                NhanVienService nv = new NhanVienService();
                nv.ThemNhanVien(nvMoi);
                MessageBox.Show("Đã Thêm Nhân Viên", "Thông Báo");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi");
            }

        }
         

        private ModelCreateNhanVien LayThongTinNhanVienTrenForm()
        {
            ModelCreateNhanVien nvA = new ModelCreateNhanVien();
            nvA.Name = txtName.Text.Trim();
            if (nvA.Name == "")
            {
                txtName.Focus();
                throw new Exception("Tên Không Để Trống");
            }
            if (nvA.Name.Length > 50)
            {
                txtName.Focus();
                txtName.SelectAll();
                throw new Exception("Tên Quá Dài");
            }
            nvA.Email = txtEmail.Text.Trim();
            nvA.Phone = txtPhone.Text.Trim();
            nvA.Username = txtUsername.Text.Trim();
            nvA.Password = txtPassword.Text.Trim();
            return nvA;
        }
    }
}
