﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFDataBase.ClassData
{
    class HangHoa
    {
        public int MaHH { get; set; }
        public string TenHH { get; set; }
        public decimal Gia { get; set; }
        public int SoLuong { get; set; }
        public string XuatSu { get; set; }
        public string MoTa { get; set; }
        public string QuyCachDongGoi { get; set; }

        public HangHoa(int maHH, string tenHH, decimal gia, int soLuong, string xuatSu, string moTa, string quyCachDongGoi)
        {
            MaHH = maHH;
            TenHH = tenHH;
            Gia = gia;
            SoLuong = soLuong;
            XuatSu = xuatSu;
            MoTa = moTa;
            QuyCachDongGoi = quyCachDongGoi;
        }
    }

    class ModelCreateHangHoa
    {
        public string TenHH { get; set; }
        public decimal Gia { get; set; }
        public int SoLuong { get; set; }
        public string XuatSu { get; set; }
        public string MoTa { get; set; }
        public string QuyCachDongGoi { get; set; }

    }

}
