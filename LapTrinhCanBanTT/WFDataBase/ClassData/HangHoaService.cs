﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace WFDataBase.ClassData
{
    class HangHoaService
    {
        private static string connectionString =
           ConfigurationManager.ConnectionStrings["WFDataBase.Properties.Settings.MyDataConnectionString"].ToString();


        public void ThemHangHoa(ModelCreateHangHoa hh) {
            string queryString = @"insert into HangHoa (TenHH, Gia, SoLuong, XuatSu, MoTa,QuyCachDongGoi) 
values(@TenHH,@Gia, @SoLuong, @XuatSu, @MoTa,@QuyCachDongGoi)";

            using (SqlConnection connection =
           new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@TenHH", hh.TenHH);
                command.Parameters.AddWithValue("@Gia", hh.Gia);
                command.Parameters.AddWithValue("@SoLuong", hh.SoLuong);
                command.Parameters.AddWithValue("@XuatSu", hh.XuatSu);
                command.Parameters.AddWithValue("@MoTa", hh.MoTa);
                command.Parameters.AddWithValue("@QuyCachDongGoi", hh.QuyCachDongGoi);
                connection.Open();
                int a = command.ExecuteNonQuery();
                connection.Close();

            }
        }

        public List<HangHoa> GetAll() {
            List<HangHoa> dsHangHoa = new List<HangHoa>();
            string queryString = @"select * from HangHoa";
            //queryString = @"select * from NhanVien";
            using (SqlConnection connection =
          new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //Console.WriteLine(String.Format("{0}", reader[0]));
                    HangHoa hh = new HangHoa(int.Parse(reader[0].ToString())
                        , reader[1].ToString(), decimal.Parse(reader[2].ToString())
                        , int.Parse(reader[3].ToString())
                        , reader[4].ToString(), reader[5].ToString()
                        , reader[6].ToString());
                    dsHangHoa.Add(hh);
                }
                connection.Close();
            }
            return dsHangHoa;
        }

        public void XoaHangHoa(string maHH)
        {
            string queryString = @"delete HangHoa where MaHH = @MaHH";

            using (SqlConnection connection =
           new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@MaHH", maHH);
                connection.Open();
                int a = command.ExecuteNonQuery();
                connection.Close();

            }
        }
    }
}
