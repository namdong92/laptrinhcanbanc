﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WFDataBase.ClassData;

namespace WFDataBase
{
    public partial class formHangHoa : Form
    {
        public formHangHoa()
        {
            InitializeComponent();
        }

        private void formHangHoa_Load(object sender, EventArgs e)
        {
            HangHoaService hhS = new HangHoaService();
            dgvHangHoa.DataSource = hhS.GetAll();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                ModelCreateHangHoa hhMoi = GetInputForm();
                HangHoaService hhS = new HangHoaService();
                hhS.ThemHangHoa(hhMoi);
                formHangHoa_Load(sender,e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
            
        }

        private ModelCreateHangHoa GetInputForm()
        {
            ModelCreateHangHoa hhMoi = new ModelCreateHangHoa();
            hhMoi.TenHH = txtTenHH.Text.Trim();
            decimal gia;
             bool kt = decimal.TryParse(txtGia.Text,out gia);
            if (kt==false) {
                throw new Exception("Giá không Đúng Định Dạng");
            }
            hhMoi.Gia = gia;
            int soLuong;
            kt = int.TryParse(txtSoLuong.Text, out soLuong);
            if (kt == false)
            {
                throw new Exception("Số Lượng không Đúng Định Dạng");
            }
            hhMoi.SoLuong = soLuong;
            hhMoi.MoTa = txtTenHH.Text.Trim();
            hhMoi.XuatSu = txtTenHH.Text.Trim();
            hhMoi.QuyCachDongGoi = txtTenHH.Text.Trim();
            return  hhMoi;
        }

        private void dgvHangHoa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var maHH = dgvHangHoa.Rows[e.RowIndex]
                .Cells[0].Value.ToString().Trim();
            var tenHH = dgvHangHoa.Rows[e.RowIndex]
                .Cells[1].Value.ToString().Trim();
            var gia = dgvHangHoa.Rows[e.RowIndex]
                .Cells[2].Value.ToString().Trim();
            var soLuong = dgvHangHoa.Rows[e.RowIndex]
                .Cells[3].Value.ToString().Trim();
            var xuatSu = dgvHangHoa.Rows[e.RowIndex]
                .Cells[4].Value.ToString().Trim();
            var moTa = dgvHangHoa.Rows[e.RowIndex]
                .Cells[5].Value.ToString().Trim();
            var quyCachDongGoi = dgvHangHoa.Rows[e.RowIndex]
                .Cells[6].Value.ToString().Trim();

            HangHoa hh = new HangHoa(int.Parse(maHH),
                tenHH, decimal.Parse(gia)
                , int.Parse(soLuong), xuatSu
                , moTa, quyCachDongGoi);
            SetInputForm(hh);

        }

        private void SetInputForm(HangHoa hh)
        {
            txtMaHH.Text = hh.MaHH.ToString();
            txtTenHH.Text = hh.TenHH.ToString();
            txtGia.Text = hh.Gia.ToString();
            txtSoLuong.Text = hh.SoLuong.ToString();
            txtXuatSu.Text = hh.XuatSu.ToString();
            txtMoTa.Text = hh.MoTa.ToString();
            txtQuyCachDongGoi.Text = hh.QuyCachDongGoi.ToString();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var maHH = txtMaHH.Text;
            try
            {
                DialogResult isOk = MessageBox.Show("Bạn Muốn Xóa Hàng Hóa Này Không?",
                    "Thông Báo", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (isOk != DialogResult.OK)
                    return;
                HangHoaService hhS = new HangHoaService();
                hhS.XoaHangHoa(maHH);
                formHangHoa_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }
    }
}
