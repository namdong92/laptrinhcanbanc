﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFDataBase
{
    class NhanVien
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }

        public NhanVien(int id, string name, 
            string phone, string email, 
            string password, string username)
        {
            Id = id;
            Name = name;
            Phone = phone;
            Email = email;
            Password = password;
            Username = username;
        }

        public NhanVien()
        {
        }
    }

    class ModelCreateNhanVien {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }

}
