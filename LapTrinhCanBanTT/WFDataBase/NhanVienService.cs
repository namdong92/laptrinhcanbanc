﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Configuration;

namespace WFDataBase
{
    class NhanVienService
    {

        private static string connectionString =
            ConfigurationManager.ConnectionStrings["WFDataBase.Properties.Settings.MyDataConnectionString"].ToString();

        public NhanVienService()
        {
        }

        public void ThemNhanVien(ModelCreateNhanVien nvMoi)
        {
            string queryString = @"insert into NhanVien (Name, Phone, Email, Username, Password) 
values(@Name,@Phone, @Email, @Username, @Password)";

            using (SqlConnection connection =
           new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Name", nvMoi.Name);
                command.Parameters.AddWithValue("@Email", nvMoi.Email);
                command.Parameters.AddWithValue("@Phone", nvMoi.Phone);
                command.Parameters.AddWithValue("@Username", nvMoi.Username);
                command.Parameters.AddWithValue("@Password", nvMoi.Password);
                connection.Open();
                int a = command.ExecuteNonQuery();
                connection.Close();

            }


        }

        public List<NhanVien> TimNhanVienTheoTen(string text)
        {
            List<NhanVien> dsNhanVien = new List<NhanVien>();
            string queryString = @"select * from NhanVien where Name like @timKiem or Phone like @timKiem";
            //queryString = @"select * from NhanVien";
            using (SqlConnection connection =
          new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@timKiem", "%"+text+"%");
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    //Console.WriteLine(String.Format("{0}", reader[0]));
                    NhanVien nv = new NhanVien(int.Parse(reader[0].ToString())
                        , reader[1].ToString(), reader[2].ToString(), reader[3].ToString()
                        , reader[4].ToString(), reader[5].ToString());
                    dsNhanVien.Add(nv);
                }

                connection.Close();

            }

            return dsNhanVien;
        }

        public void SuaNhanVien(NhanVien nvMoi)
        {
            string queryString = @"update NhanVien set 
Name = @name, 
Phone = @Phone, 
Email = @Email, 
Username = @Username, 
Password = @Password
where Id = @Id";

            using (SqlConnection connection =
           new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Id", nvMoi.Id);
                command.Parameters.AddWithValue("@Name", nvMoi.Name);
                command.Parameters.AddWithValue("@Email", nvMoi.Email);
                command.Parameters.AddWithValue("@Phone", nvMoi.Phone);
                command.Parameters.AddWithValue("@Username", nvMoi.Username);
                command.Parameters.AddWithValue("@Password", nvMoi.Password);
                connection.Open();
                int a = command.ExecuteNonQuery();
                connection.Close();

            }
        }

        public void XoaNhanVien(string id)
        {
            string queryString = @"delete NhanVien where Id = @Id";

            using (SqlConnection connection =
           new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Id", id);
                connection.Open();
                int a = command.ExecuteNonQuery();
                connection.Close();

            }
        }
    }
}