﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class formPhepTru : Form
    {
        public formPhepTru()
        {
            InitializeComponent();
        }

        private void txtSoA_TextChanged(object sender, EventArgs e)
        {

        }


        private PhepTinh GetInputForm()
        {
            double a, b;
            bool kt = double.TryParse(txtSoA.Text, out a);
            if (kt == false)
            {
                txtSoA.Focus();
                txtSoA.SelectAll();
                throw new Exception("Số A Không Đúng Định Dạng");
            }

            kt = double.TryParse(txtSoB.Text, out b);
            if (kt == false)
            {
                txtSoB.Focus();
                txtSoB.SelectAll();

                throw new Exception("Số B Không Đúng Định Dạng");
            }

            return new PhepTinh(a, b);
        }
        private void btnTinh_Click(object sender, EventArgs e)
        {
            try
            {
                PhepTinh pt = GetInputForm();
                lblKetQua.Text = "Kết Quả: " + pt.Tru().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
    }
}
