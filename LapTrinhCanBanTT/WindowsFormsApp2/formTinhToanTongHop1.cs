﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class formTinhToanTongHop1 : Form
    {
        public formTinhToanTongHop1()
        {
            InitializeComponent();
        }

        private PhepTinh GetInputForm()
        {
            double a, b;
            bool kt = double.TryParse(txtSoA.Text, out a);
            if (kt == false)
            {
                txtSoA.Focus();
                txtSoA.SelectAll();
                throw new Exception("Số A Không Đúng Định Dạng");
            }

            kt = double.TryParse(txtSoB.Text, out b);
            if (kt == false)
            {
                txtSoB.Focus();
                txtSoB.SelectAll();

                throw new Exception("Số B Không Đúng Định Dạng");
            }

            return new PhepTinh(a, b);
        }

        private void btnTong_Click(object sender, EventArgs e)
        {

            try
            {
                PhepTinh pt = this.GetInputForm();
                lblKetQua.Text = pt.Cong().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnHieu_Click(object sender, EventArgs e)
        {
            try
            {
                PhepTinh pt = this.GetInputForm();
                lblKetQua.Text = pt.Tru().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTich_Click(object sender, EventArgs e)
        {
            try
            {
                PhepTinh pt = this.GetInputForm();
                lblKetQua.Text = pt.Nhan().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnThuong_Click(object sender, EventArgs e)
        {
            try
            {
                PhepTinh pt = this.GetInputForm();
                if (pt.soB == 0) {
                    throw new Exception("B Không Thể Bằng 0");
                }
                lblKetQua.Text = pt.Chia().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void formTinhToanTongHop1_Load(object sender, EventArgs e)
        {
            PhepTinh pt = new PhepTinh();
            //foreach (var item in pt.DanhSachLoaiPt())
            //{
            //    cbbPhepTinh.Items.Add(item);
            //}


            cbbPhepTinh.DataSource = pt.DanhSachLoaiPt();
            cbbPhepTinh.ValueMember = "Id";
            cbbPhepTinh.DisplayMember = "Name";



        }

        private void button1_Click(object sender, EventArgs e)
        {


            LoaiPhepTinh lPt = (LoaiPhepTinh)cbbPhepTinh.SelectedItem;

            switch (lPt.Id) {
                case 1:
                    btnTong_Click(sender, e);
                    break;
                case 2:
                    btnHieu_Click(sender, e);
                    break;
                case 3:
                    btnTich_Click(sender, e);
                    break;
                case 4:
                    btnThuong_Click(sender, e);
                    break;

            }

        }
    }
}
