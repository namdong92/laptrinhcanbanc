﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class FormPhepChia : Form
    {
        public FormPhepChia()
        {
            InitializeComponent();
        }

        private PhepTinh GetInputForm()
        {
            double a, b;
            bool kt = double.TryParse(txtSoA.Text, out a);
            if (kt == false)
            {
                txtSoA.Focus();
                txtSoA.SelectAll();
                throw new Exception("Số A Không Đúng Định Dạng");
            }

            kt = double.TryParse(txtSoB.Text, out b);
            if (kt == false)
            {
                txtSoB.Focus();
                txtSoB.SelectAll();

                throw new Exception("Số B Không Đúng Định Dạng");
            }
            if (b == 0) {
                txtSoB.Focus();
                txtSoB.SelectAll();
                throw new Exception("Số B Không Được Bằng 0");
            }

            return new PhepTinh(a, b);
        }

        private void btnTinh_Click(object sender, EventArgs e)
        {
            try
            {
                PhepTinh pt = GetInputForm();
                lblKetQua.Text = "Kết Quả: " + pt.Chia().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
