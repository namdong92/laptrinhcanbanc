﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp2
{
    class LoaiPhepTinh
    {
       

        public int Id { get; set; }
        public string Name { get; set; }
        public static int Cong = 1;
        public static int Tru = 2;
        public static int Nhan = 3;
        public static int Chia = 4;

        public LoaiPhepTinh(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public List<LoaiPhepTinh> DSLoaiPhepTinh() {
            List<LoaiPhepTinh> a = new List<LoaiPhepTinh>();
            a.Add(new LoaiPhepTinh(Cong, "Cộng"));
            a.Add(new LoaiPhepTinh(Tru, "Trừ"));
            a.Add(new LoaiPhepTinh(Nhan, "Nhân"));
            a.Add(new LoaiPhepTinh(Chia, "Chia"));
            
            return a;
        }

    }
}
