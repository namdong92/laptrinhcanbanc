﻿namespace WindowsFormsApp2
{
    partial class formTinhToanTongHop1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKetQua = new System.Windows.Forms.Label();
            this.btnTong = new System.Windows.Forms.Button();
            this.txtSoB = new System.Windows.Forms.TextBox();
            this.txtSoA = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnHieu = new System.Windows.Forms.Button();
            this.btnTich = new System.Windows.Forms.Button();
            this.btnThuong = new System.Windows.Forms.Button();
            this.cbbPhepTinh = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblKetQua
            // 
            this.lblKetQua.AutoSize = true;
            this.lblKetQua.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKetQua.ForeColor = System.Drawing.Color.Maroon;
            this.lblKetQua.Location = new System.Drawing.Point(269, 242);
            this.lblKetQua.Name = "lblKetQua";
            this.lblKetQua.Size = new System.Drawing.Size(85, 25);
            this.lblKetQua.TabIndex = 29;
            this.lblKetQua.Text = "Kết Quả";
            // 
            // btnTong
            // 
            this.btnTong.Location = new System.Drawing.Point(129, 162);
            this.btnTong.Name = "btnTong";
            this.btnTong.Size = new System.Drawing.Size(54, 46);
            this.btnTong.TabIndex = 28;
            this.btnTong.Text = "Tồng";
            this.btnTong.UseVisualStyleBackColor = true;
            this.btnTong.Click += new System.EventHandler(this.btnTong_Click);
            // 
            // txtSoB
            // 
            this.txtSoB.Location = new System.Drawing.Point(481, 106);
            this.txtSoB.Name = "txtSoB";
            this.txtSoB.Size = new System.Drawing.Size(198, 20);
            this.txtSoB.TabIndex = 27;
            // 
            // txtSoA
            // 
            this.txtSoA.Location = new System.Drawing.Point(58, 106);
            this.txtSoA.Name = "txtSoA";
            this.txtSoA.Size = new System.Drawing.Size(198, 20);
            this.txtSoA.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(478, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Số B";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Số A";
            // 
            // btnHieu
            // 
            this.btnHieu.Location = new System.Drawing.Point(240, 162);
            this.btnHieu.Name = "btnHieu";
            this.btnHieu.Size = new System.Drawing.Size(54, 46);
            this.btnHieu.TabIndex = 30;
            this.btnHieu.Text = "Hiệu";
            this.btnHieu.UseVisualStyleBackColor = true;
            this.btnHieu.Click += new System.EventHandler(this.btnHieu_Click);
            // 
            // btnTich
            // 
            this.btnTich.Location = new System.Drawing.Point(350, 162);
            this.btnTich.Name = "btnTich";
            this.btnTich.Size = new System.Drawing.Size(54, 46);
            this.btnTich.TabIndex = 31;
            this.btnTich.Text = "Tích";
            this.btnTich.UseVisualStyleBackColor = true;
            this.btnTich.Click += new System.EventHandler(this.btnTich_Click);
            // 
            // btnThuong
            // 
            this.btnThuong.Location = new System.Drawing.Point(459, 162);
            this.btnThuong.Name = "btnThuong";
            this.btnThuong.Size = new System.Drawing.Size(54, 46);
            this.btnThuong.TabIndex = 32;
            this.btnThuong.Text = "Thương";
            this.btnThuong.UseVisualStyleBackColor = true;
            this.btnThuong.Click += new System.EventHandler(this.btnThuong_Click);
            // 
            // cbbPhepTinh
            // 
            this.cbbPhepTinh.FormattingEnabled = true;
            this.cbbPhepTinh.Items.AddRange(new object[] {
            "Cộng",
            "Trừ ",
            "Nhân",
            "Chia"});
            this.cbbPhepTinh.Location = new System.Drawing.Point(302, 106);
            this.cbbPhepTinh.Name = "cbbPhepTinh";
            this.cbbPhepTinh.Size = new System.Drawing.Size(121, 21);
            this.cbbPhepTinh.TabIndex = 33;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(570, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 46);
            this.button1.TabIndex = 34;
            this.button1.Text = "Tính";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // formTinhToanTongHop1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbbPhepTinh);
            this.Controls.Add(this.btnThuong);
            this.Controls.Add(this.btnTich);
            this.Controls.Add(this.btnHieu);
            this.Controls.Add(this.lblKetQua);
            this.Controls.Add(this.btnTong);
            this.Controls.Add(this.txtSoB);
            this.Controls.Add(this.txtSoA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "formTinhToanTongHop1";
            this.Text = "formTinhToanTongHop1";
            this.Load += new System.EventHandler(this.formTinhToanTongHop1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKetQua;
        private System.Windows.Forms.Button btnTong;
        private System.Windows.Forms.TextBox txtSoB;
        private System.Windows.Forms.TextBox txtSoA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnHieu;
        private System.Windows.Forms.Button btnTich;
        private System.Windows.Forms.Button btnThuong;
        private System.Windows.Forms.ComboBox cbbPhepTinh;
        private System.Windows.Forms.Button button1;
    }
}