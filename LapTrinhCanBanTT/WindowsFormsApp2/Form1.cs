﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGui_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Xin chao", "Thong bao"
                , MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
           var res =   MessageBox.Show("Bạn có muốn thoát không?","Thông Báo"
                ,MessageBoxButtons.YesNoCancel,MessageBoxIcon.Warning);

            if (res != DialogResult.Yes) {
                e.Cancel = true;
            }
                
        }

        private void btnMayTinh_Click(object sender, EventArgs e)
        {
            Form formMayTinh = new formMayTinh();
            formMayTinh.Show();
        }
    }
}
