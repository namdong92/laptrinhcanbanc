﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class formDangNhap : Form
    {
        public formDangNhap()
        {
            InitializeComponent();
        }

        private void formDangNhap_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formDangNhap_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult kt = MessageBox
                .Show("Bạn có muốn thoát không?"
                , "Thông báo"
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Warning);

            if (kt == DialogResult.Cancel) {
                e.Cancel = false;
            }
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            string taiKhoan = txtTaiKhoan.Text;
            string matKhau = txtMatKhau.Text;
            if(taiKhoan == "")
            {
                MessageBox.Show("Tài khoản Không Để Trống");
                txtTaiKhoan.Focus();
                return;
            }
            if (matKhau == "")
            {
                MessageBox.Show("Mật Khẩu Không Để Trống");
                txtMatKhau.Focus();
                return;
            }

            if (txtMatKhau.Text == "123" && txtTaiKhoan.Text == "admin")
            {
                Form fMain = new Form1();
                fMain.Show();
            }
            else {
                MessageBox.Show("Mật Khẩu/ Tài Khoản Không Đúng");
                txtTaiKhoan.Focus();
                return;
            }

        }

        private void formDangNhap_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter) {
                btnDangNhap_Click(sender, e);
            }
        }
    }
}
