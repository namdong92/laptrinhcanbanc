﻿namespace WindowsFormsApp2
{
    partial class formMayTinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cácPhépTínhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phépCộngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phépTrừToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phépNhânToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phépChiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thoátToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tổngHợp1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hIenMayTinhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wordpadexeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cácPhépTínhToolStripMenuItem,
            this.thoátToolStripMenuItem,
            this.hIenMayTinhToolStripMenuItem,
            this.wordpadexeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cácPhépTínhToolStripMenuItem
            // 
            this.cácPhépTínhToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.phépCộngToolStripMenuItem,
            this.phépTrừToolStripMenuItem,
            this.phépNhânToolStripMenuItem,
            this.phépChiaToolStripMenuItem,
            this.tổngHợp1ToolStripMenuItem,
            this.calculatorToolStripMenuItem});
            this.cácPhépTínhToolStripMenuItem.Name = "cácPhépTínhToolStripMenuItem";
            this.cácPhépTínhToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.cácPhépTínhToolStripMenuItem.Text = "Các Phép Tính";
            // 
            // phépCộngToolStripMenuItem
            // 
            this.phépCộngToolStripMenuItem.Name = "phépCộngToolStripMenuItem";
            this.phépCộngToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.phépCộngToolStripMenuItem.Text = "Phép Cộng";
            this.phépCộngToolStripMenuItem.Click += new System.EventHandler(this.phépCộngToolStripMenuItem_Click);
            // 
            // phépTrừToolStripMenuItem
            // 
            this.phépTrừToolStripMenuItem.Name = "phépTrừToolStripMenuItem";
            this.phépTrừToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.phépTrừToolStripMenuItem.Text = "Phép Trừ";
            this.phépTrừToolStripMenuItem.Click += new System.EventHandler(this.phépTrừToolStripMenuItem_Click);
            // 
            // phépNhânToolStripMenuItem
            // 
            this.phépNhânToolStripMenuItem.Name = "phépNhânToolStripMenuItem";
            this.phépNhânToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.phépNhânToolStripMenuItem.Text = "Phép Nhân";
            this.phépNhânToolStripMenuItem.Click += new System.EventHandler(this.phépNhânToolStripMenuItem_Click);
            // 
            // phépChiaToolStripMenuItem
            // 
            this.phépChiaToolStripMenuItem.Name = "phépChiaToolStripMenuItem";
            this.phépChiaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.phépChiaToolStripMenuItem.Text = "Phép Chia";
            this.phépChiaToolStripMenuItem.Click += new System.EventHandler(this.phépChiaToolStripMenuItem_Click);
            // 
            // thoátToolStripMenuItem
            // 
            this.thoátToolStripMenuItem.Name = "thoátToolStripMenuItem";
            this.thoátToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.thoátToolStripMenuItem.Text = "Thoát";
            this.thoátToolStripMenuItem.Click += new System.EventHandler(this.thoátToolStripMenuItem_Click);
            // 
            // tổngHợp1ToolStripMenuItem
            // 
            this.tổngHợp1ToolStripMenuItem.Name = "tổngHợp1ToolStripMenuItem";
            this.tổngHợp1ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tổngHợp1ToolStripMenuItem.Text = "Tổng Hợp 1";
            this.tổngHợp1ToolStripMenuItem.Click += new System.EventHandler(this.tổngHợp1ToolStripMenuItem_Click);
            // 
            // hIenMayTinhToolStripMenuItem
            // 
            this.hIenMayTinhToolStripMenuItem.Name = "hIenMayTinhToolStripMenuItem";
            this.hIenMayTinhToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.hIenMayTinhToolStripMenuItem.Text = "HIen May Tinh";
            this.hIenMayTinhToolStripMenuItem.Click += new System.EventHandler(this.hIenMayTinhToolStripMenuItem_Click);
            // 
            // wordpadexeToolStripMenuItem
            // 
            this.wordpadexeToolStripMenuItem.Name = "wordpadexeToolStripMenuItem";
            this.wordpadexeToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.wordpadexeToolStripMenuItem.Text = "wordpad.exe";
            this.wordpadexeToolStripMenuItem.Click += new System.EventHandler(this.wordpadexeToolStripMenuItem_Click);
            // 
            // calculatorToolStripMenuItem
            // 
            this.calculatorToolStripMenuItem.Name = "calculatorToolStripMenuItem";
            this.calculatorToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.calculatorToolStripMenuItem.Text = "Calculator";
            this.calculatorToolStripMenuItem.Click += new System.EventHandler(this.calculatorToolStripMenuItem_Click);
            // 
            // formMayTinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "formMayTinh";
            this.Text = "formMayTinh";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formMayTinh_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cácPhépTínhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phépCộngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phépTrừToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phépNhânToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phépChiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thoátToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tổngHợp1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hIenMayTinhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wordpadexeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem;
    }
}