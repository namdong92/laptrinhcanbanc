﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class formMayTinh : Form
    {
        public formMayTinh()
        {
            InitializeComponent();
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void formMayTinh_FormClosing(object sender, FormClosingEventArgs e)
        {
            var res = MessageBox.Show("Bạn có muốn thoát không?", "Thông Báo"
               , MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void phépCộngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formPhepCong = new formPhepCong();
            formPhepCong.MdiParent = this;
            formPhepCong.Show();
            

        }

        private void phépTrừToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formPhepTru = new formPhepTru();
            formPhepTru.MdiParent = this;
            formPhepTru.Show();
        }

        private void phépNhânToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formPhepNhan = new formPhepNhan();
            formPhepNhan.MdiParent = this;
            formPhepNhan.Show();
        }

        private void phépChiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formPhepChia = new FormPhepChia();
            formPhepChia.MdiParent = this;
            formPhepChia.Show();
        }

        private void tổngHợp1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formPhepChia = new formTinhToanTongHop1();
            formPhepChia.MdiParent = this;
            formPhepChia.Show();
        }

        private void hIenMayTinhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var p  = System.Diagnostics.Process.Start("calc.exe");
            p.WaitForInputIdle();
            
        }

        private void wordpadexeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var p = System.Diagnostics.Process.Start("wordpad.exe");
            p.WaitForInputIdle();
            
        }

        private void calculatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formPhepChia = new formCalculator();
            formPhepChia.MdiParent = this;
            formPhepChia.Show();
        }
    }
}
