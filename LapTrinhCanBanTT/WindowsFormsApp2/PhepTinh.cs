﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApp2
{
    class PhepTinh
    {
        public double soA { get; set; }
        public double soB { get; set; }

        public PhepTinh(double soA, double soB)
        {
            this.soA = soA;
            this.soB = soB;
        }

        public PhepTinh()
        {
        }

        public double Cong() {
            return soA + soB;
        }
        public double Tru()
        {
            return soA - soB;
        }
        public double Nhan()
        {
            return soA * soB;
        }
        public double Chia()
        {
            return soA / soB;
        }

        public List<LoaiPhepTinh> DanhSachLoaiPt() {
            List<LoaiPhepTinh> a = new List<LoaiPhepTinh>();
            a.Add(new LoaiPhepTinh(1, "Cộng"));
            a.Add(new LoaiPhepTinh(2, "Trừ"));
            a.Add(new LoaiPhepTinh(3, "Nhân"));
            a.Add(new LoaiPhepTinh(4, "Chia"));
            return a;
        }

    }
}
