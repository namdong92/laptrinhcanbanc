﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class formCalculator : Form
    {
        public static double ketQua = 0;

        public static int tenPhepTinh = 0;


        public formCalculator()
        {
            InitializeComponent();
        }



        private void btnTong_Click(object sender, EventArgs e)
        {

            double a;
            double.TryParse(txtSo.Text, out a);
            if (tenPhepTinh == 0)
                ketQua = a;
            if (tenPhepTinh > 0)
                ketQua += a;
            ResetTxtSo("0");
            tenPhepTinh = LoaiPhepTinh.Cong;


        }

        private void btnHieu_Click(object sender, EventArgs e)
        {
            double a;
            double.TryParse(txtSo.Text, out a);
            if (tenPhepTinh == 0)
                ketQua = a;
            if (tenPhepTinh > 0)
                ketQua = ketQua - a;
            ResetTxtSo("0");
            tenPhepTinh = LoaiPhepTinh.Tru;
        }

        private void ResetTxtSo(string v)
        {
            txtSo.Text = v;
            txtSo.Focus();
            txtSo.SelectAll();
        }

        private void btnTich_Click(object sender, EventArgs e)
        {
            double a;
            double.TryParse(txtSo.Text, out a);
            if (tenPhepTinh == 0)
            {
                ketQua = a;
            }

            if (tenPhepTinh > 0)
            {
                ketQua *= a;
            }


            ResetTxtSo("0");
            tenPhepTinh = LoaiPhepTinh.Nhan;
        }

        private void btnThuong_Click(object sender, EventArgs e)
        {
            double a;
            double.TryParse(txtSo.Text, out a);
            if (tenPhepTinh == 0)
            {
                ketQua = a;
            }

            if (tenPhepTinh > 0)
                ketQua /= a;
            ResetTxtSo("0");
            tenPhepTinh = LoaiPhepTinh.Chia;
        }
        /// <summary>
        /// dau bang
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            double b;
            double.TryParse(txtSo.Text, out b);
            PhepTinh pt = new PhepTinh(ketQua, b);

            if (tenPhepTinh == LoaiPhepTinh.Tru)
            {
                ketQua = pt.Tru();
                txtSo.Text = ketQua.ToString();
            }
            if (tenPhepTinh == LoaiPhepTinh.Cong)
            {
                ketQua = pt.Cong();
                txtSo.Text = ketQua.ToString();

            }
            if (tenPhepTinh == LoaiPhepTinh.Nhan)
            {
                ketQua = pt.Nhan();
                txtSo.Text = ketQua.ToString();
            }
            if (tenPhepTinh == LoaiPhepTinh.Chia)
            {
                ketQua = pt.Chia();
                txtSo.Text = ketQua.ToString();
            }
            tenPhepTinh = 0;
            ResetTxtSo(ketQua.ToString());

        }

        private void formCalculator_Load(object sender, EventArgs e)
        {

        }

        private void btnAC_Click(object sender, EventArgs e)
        {
            ketQua = 0;
            ResetTxtSo("0");
        }

        private void btnTong_KeyUp(object sender, KeyEventArgs e)
        {
            MessageBox.Show(e.KeyCode.ToString());
        }

        private void formCalculator_KeyUp(object sender, KeyEventArgs e)
        {
            MessageBox.Show(e.KeyCode.ToString());
        }

        private void txtSo_KeyUp(object sender, KeyEventArgs e)
        {


        }

        private void txtSo_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void txtSo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSo_KeyPress(object sender, KeyPressEventArgs e)
        {

            //MessageBox.Show(e.KeyChar.ToString());
            if (e.KeyChar == '+')
            {
                btnTong_Click(sender, e);
                e.Handled = true;
            }
            if (e.KeyChar == '-')
            {
                btnHieu_Click(sender, e);
                e.Handled = true;
            }
            if (e.KeyChar == '*')
            {
                btnTich_Click(sender, e);
                e.Handled = true;
            }
            if (e.KeyChar == '/')
            {
                btnThuong_Click(sender, e);
                e.Handled = true;
            }
            if (e.KeyChar == (char)13)
            {
                button1_Click(sender, e);
                e.Handled = true;
            }
          //  MessageBox.Show(e.KeyChar.ToString());

            //if ((e.KeyChar >= 48 && e.KeyValue <= 57) ||
            //   (e.KeyValue >= 96 && e.KeyValue <= 105))
            //{
            //    //MessageBox.Show(e.KeyValue.ToString());
            //}
            //if (e.KeyValue == 107 || e.KeyValue == 16)
            //{
            //    //cong

            //    btnTong_Click(sender, e);
            //    e.Handled = false;
            //}
        }
    }
}
