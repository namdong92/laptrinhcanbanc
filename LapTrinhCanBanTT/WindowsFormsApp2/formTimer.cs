﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class formTimer : Form
    {
        public formTimer()
        {
            InitializeComponent();

        }

        private void timerAbc_Tick(object sender, EventArgs e)
        {
            timerAbc.Interval = 100;

         
            Point pxr = new Point(ptbXuongRong.Location.X,
                ptbXuongRong.Location.Y);
            pxr.X -= 10;
            if (pxr.X <= 0) {
                pxr.X = this.Width;
            }

            ptbXuongRong.Location = pxr;
            
            VaCham(ptbKhungLong,ptbXuongRong);

            ChuyenDongCuaKhungLong();

            //  MessageBox.Show(DateTime.Now.ToString());
        }

        private void VaCham(PictureBox ptbKhungLong, PictureBox ptbXuongRong)
        {
            var b = ptbXuongRong.Location;
            var a = ptbKhungLong.Location;
            // kiem tra x
            if (b.X <= a.X + ptbKhungLong.Width)
            {
                // kiem tra y
                if ((b.Y > a.Y && b.Y < a.Y + ptbKhungLong.Height) ||
                    (b.Y + ptbXuongRong.Height > a.Y
                    && b.Y + ptbXuongRong.Height < a.Y + ptbKhungLong.Height)
                    || a.Y == b.Y
                    )
                {
                    timerAbc.Stop();
                }

            }
            


        }

        /// <summary>
        /// chuyen dong cua khung long
        /// </summary>
        /// <param name="huong">1 xuong -1 la len</param>
        private void ChuyenDongCuaKhungLong(int huong = 1)
        {

            Point p = new Point(ptbKhungLong.Location.X,
                            ptbKhungLong.Location.Y);
            p.Y = p.Y + (10 * huong);
            if (p.Y >= 150) {
                p.Y = 150;
            }
            if (p.Y <= 0)
            {
                p.Y = 0;
            }
            ptbKhungLong.Location = p;
        }

        private void formTimer_Load(object sender, EventArgs e)
        {
            timerAbc.Start();
            ptbKhungLong.Location = new Point(0 ,150);
            ptbXuongRong.Location = new Point(this.Width-ptbXuongRong.Width, 150);
        }

        private void formTimer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up) {
                ChuyenDongCuaKhungLong(-4);
            }
            if (e.KeyCode == Keys.Down)
            {
                ChuyenDongCuaKhungLong(1);
            }
        }

        private void btnChoiLai_Click(object sender, EventArgs e)
        {
            formTimer_Load(sender, e);
        }

        private void btnChoiLoai_Click(object sender, EventArgs e)
        {
            formTimer_Load(sender, e);
        }

        private void formTimer_Click(object sender, EventArgs e)
        {
            ChuyenDongCuaKhungLong(-4);
        }
    }
}
