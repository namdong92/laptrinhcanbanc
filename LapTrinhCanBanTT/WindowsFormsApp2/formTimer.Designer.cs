﻿namespace WindowsFormsApp2
{
    partial class formTimer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerAbc = new System.Windows.Forms.Timer(this.components);
            this.ptbXuongRong = new System.Windows.Forms.PictureBox();
            this.ptbKhungLong = new System.Windows.Forms.PictureBox();
            this.btnChoiLoai = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ptbXuongRong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbKhungLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnChoiLoai)).BeginInit();
            this.SuspendLayout();
            // 
            // timerAbc
            // 
            this.timerAbc.Tick += new System.EventHandler(this.timerAbc_Tick);
            // 
            // ptbXuongRong
            // 
            this.ptbXuongRong.Image = global::WindowsFormsApp2.Properties.Resources.khung_long_play;
            this.ptbXuongRong.Location = new System.Drawing.Point(394, 168);
            this.ptbXuongRong.Name = "ptbXuongRong";
            this.ptbXuongRong.Size = new System.Drawing.Size(42, 40);
            this.ptbXuongRong.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbXuongRong.TabIndex = 1;
            this.ptbXuongRong.TabStop = false;
            // 
            // ptbKhungLong
            // 
            this.ptbKhungLong.Image = global::WindowsFormsApp2.Properties.Resources.dino;
            this.ptbKhungLong.Location = new System.Drawing.Point(87, 168);
            this.ptbKhungLong.Name = "ptbKhungLong";
            this.ptbKhungLong.Size = new System.Drawing.Size(42, 40);
            this.ptbKhungLong.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ptbKhungLong.TabIndex = 0;
            this.ptbKhungLong.TabStop = false;
            // 
            // btnChoiLoai
            // 
            this.btnChoiLoai.Image = global::WindowsFormsApp2.Properties.Resources.khung_long_play;
            this.btnChoiLoai.Location = new System.Drawing.Point(613, 316);
            this.btnChoiLoai.Name = "btnChoiLoai";
            this.btnChoiLoai.Size = new System.Drawing.Size(42, 40);
            this.btnChoiLoai.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnChoiLoai.TabIndex = 2;
            this.btnChoiLoai.TabStop = false;
            this.btnChoiLoai.Click += new System.EventHandler(this.btnChoiLoai_Click);
            // 
            // formTimer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnChoiLoai);
            this.Controls.Add(this.ptbXuongRong);
            this.Controls.Add(this.ptbKhungLong);
            this.Name = "formTimer";
            this.Text = "formTimer";
            this.Load += new System.EventHandler(this.formTimer_Load);
            this.Click += new System.EventHandler(this.formTimer_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.formTimer_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ptbXuongRong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ptbKhungLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnChoiLoai)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerAbc;
        private System.Windows.Forms.PictureBox ptbKhungLong;
        private System.Windows.Forms.PictureBox ptbXuongRong;
        private System.Windows.Forms.PictureBox btnChoiLoai;
    }
}