﻿namespace WindowsFormsApp2
{
    partial class formCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnThuong = new System.Windows.Forms.Button();
            this.btnTich = new System.Windows.Forms.Button();
            this.btnHieu = new System.Windows.Forms.Button();
            this.btnTong = new System.Windows.Forms.Button();
            this.txtSo = new System.Windows.Forms.TextBox();
            this.btnAC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(456, 79);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 46);
            this.button1.TabIndex = 44;
            this.button1.Text = "=";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnThuong
            // 
            this.btnThuong.Location = new System.Drawing.Point(345, 79);
            this.btnThuong.Name = "btnThuong";
            this.btnThuong.Size = new System.Drawing.Size(54, 46);
            this.btnThuong.TabIndex = 42;
            this.btnThuong.Text = "Chia";
            this.btnThuong.UseVisualStyleBackColor = true;
            this.btnThuong.Click += new System.EventHandler(this.btnThuong_Click);
            // 
            // btnTich
            // 
            this.btnTich.Location = new System.Drawing.Point(236, 79);
            this.btnTich.Name = "btnTich";
            this.btnTich.Size = new System.Drawing.Size(54, 46);
            this.btnTich.TabIndex = 41;
            this.btnTich.Text = "Nhân";
            this.btnTich.UseVisualStyleBackColor = true;
            this.btnTich.Click += new System.EventHandler(this.btnTich_Click);
            // 
            // btnHieu
            // 
            this.btnHieu.Location = new System.Drawing.Point(126, 79);
            this.btnHieu.Name = "btnHieu";
            this.btnHieu.Size = new System.Drawing.Size(54, 46);
            this.btnHieu.TabIndex = 40;
            this.btnHieu.Text = "Trừ";
            this.btnHieu.UseVisualStyleBackColor = true;
            this.btnHieu.Click += new System.EventHandler(this.btnHieu_Click);
            // 
            // btnTong
            // 
            this.btnTong.Location = new System.Drawing.Point(15, 79);
            this.btnTong.Name = "btnTong";
            this.btnTong.Size = new System.Drawing.Size(54, 46);
            this.btnTong.TabIndex = 39;
            this.btnTong.Text = "Cộng";
            this.btnTong.UseVisualStyleBackColor = true;
            this.btnTong.Click += new System.EventHandler(this.btnTong_Click);
            this.btnTong.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnTong_KeyUp);
            // 
            // txtSo
            // 
            this.txtSo.Location = new System.Drawing.Point(15, 12);
            this.txtSo.Multiline = true;
            this.txtSo.Name = "txtSo";
            this.txtSo.Size = new System.Drawing.Size(495, 46);
            this.txtSo.TabIndex = 37;
            this.txtSo.TextChanged += new System.EventHandler(this.txtSo_TextChanged);
            this.txtSo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSo_KeyDown);
            this.txtSo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSo_KeyPress);
            this.txtSo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSo_KeyUp);
            // 
            // btnAC
            // 
            this.btnAC.Location = new System.Drawing.Point(15, 149);
            this.btnAC.Name = "btnAC";
            this.btnAC.Size = new System.Drawing.Size(54, 46);
            this.btnAC.TabIndex = 45;
            this.btnAC.Text = "AC";
            this.btnAC.UseVisualStyleBackColor = true;
            this.btnAC.Click += new System.EventHandler(this.btnAC_Click);
            // 
            // formCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 450);
            this.Controls.Add(this.btnAC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnThuong);
            this.Controls.Add(this.btnTich);
            this.Controls.Add(this.btnHieu);
            this.Controls.Add(this.btnTong);
            this.Controls.Add(this.txtSo);
            this.Name = "formCalculator";
            this.Text = "formCalculator";
            this.Load += new System.EventHandler(this.formCalculator_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.formCalculator_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnThuong;
        private System.Windows.Forms.Button btnTich;
        private System.Windows.Forms.Button btnHieu;
        private System.Windows.Forms.Button btnTong;
        private System.Windows.Forms.TextBox txtSo;
        private System.Windows.Forms.Button btnAC;
    }
}